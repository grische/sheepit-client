package com.sheepit.client.hardware.gpu.nvidia;

//https://docs.nvidia.com/deploy/nvml-api/group__nvmlDeviceEnumvs.html
public class NVMLResult {
	public static final int NVML_SUCCESS = 0;
	public static final int NVML_ERROR_UNITIALIZED = 1;	//NVML was not initialized with nvmlInit()
	public static final int NVML_ERROR_INVALID_ARGUMENT = 2;
	public static final int NVML_ERROR_INSUFFICIENT_SIZE = 7;
}
